
from data_processing import DataProcessing
from model import Model
import pickle

from sklearn.metrics import accuracy_score

#param_grid = {'C': [0.01, 0.1, 1, 10, 100], 'penalty': ['l1', 'l2']}

if __name__ == '__main__':

    ### i want to test the baseline NLP ....

    # load data
    # data = pandas.read_csv('data/opinions.csv', sep='\t+', engine='python')
    # data['"opinion'] = data['"opinion'].str[1:]
    # data['"opinion'] = data['"opinion'].astype(int)
    # data.columns = ['opinion', 'text']
    #
    # #############################################################################
    # # data pre-precessing
    # processor = DataProcessing()
    #
    # data_train, data_test, data_val = processor.data_splitting_train_test_val(data)
    #
    # data_train = processor.remove_stopword(data_train)
    # data_train = processor.stem_words(data_train)
    # data_train = processor.text_sentence(data_train)
    #
    # data_test = processor.remove_stopword(data_test)
    # data_test = processor.stem_words(data_test)
    # data_test = processor.text_sentence(data_test)
    #
    # # compute tf-idf signatures
    # mat_tf_idf_train, mat_tf_idf_test = processor.create_tf_idf_matrix(data_train, data_test)

    # load the content

    data_train = pickle.load(open("C:/Users/jerome.divac/Stage/NLP/tf_idf/data_train.pkl", "rb"))
    data_test = pickle.load(open("C:/Users/jerome.divac/Stage/NLP/tf_idf/data_test.pkl", "rb"))

    mat_tf_idf_train = pickle.load(open("C:/Users/jerome.divac/Stage/NLP/tf_idf/mat_tf_idf_train.pkl", "rb"))
    mat_tf_idf_test = pickle.load(open("C:/Users/jerome.divac/Stage/NLP/tf_idf/mat_tf_idf_test.pkl", "rb"))

    #############################################################################
    # test logistic regression model
    my_model = Model('LogReg')
    my_model.fit_model(mat_tf_idf_train, data_train['opinion'])

    score_ = accuracy_score(my_model.predict_model(mat_tf_idf_train), data_train['opinion'])
    print(score_)


    my_model.compute_accuracy(mat_tf_idf_train, data_train)

    my_model.compute_accuracy(mat_tf_idf_test, data_test)

    my_model.cross_validation(mat_tf_idf_train, data_train)

    my_model.compute_grid_search(my_model.param_grid, mat_tf_idf_train, data_train)

    my_model.plot_histo(mat_tf_idf_train, data_train)

    my_model.compute_min_max_df(data_train, data_test)



