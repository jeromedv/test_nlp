import pandas

import nltk
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from nltk.stem.porter import PorterStemmer
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from sklearn.model_selection import train_test_split

# this option is useful to display the dataframe with all the columns

import pandas as pd
import numpy as np
desired_width=320
pd.set_option('display.width', desired_width)
np.set_printoptions(linewidth=desired_width)
pd.set_option('display.max_columns',20)


# this bloc of imports is used with the logistic regression/SVM Model

import matplotlib.pyplot as plt
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn import svm
from sklearn.svm import LinearSVC
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score
# from sklearn.cross_validation import cross_val_score
from sklearn.model_selection import cross_val_score
from sklearn.metrics import r2_score


# read the data from csv file

data = pandas.read_csv('data/opinions.csv', sep = '\t+', engine='python')

print(data.columns)
print(data.head(10))



# modification of the columns names
#data['"opinion'] = data['"opinion'].str.replace('"','')
#data['text"'] = data['text"'].str.replace('"','')
#data['"opinion'] = data['"opinion'].apply(pandas.to_numeric, errors='coerce')


# this command serves to extract the labels as 0 and 1 without the quotes "

data['"opinion'] = data['"opinion'].str[1:]
data['"opinion'] = data['"opinion'].astype(int)

print(data.head(5))


data.columns = ['opinion', 'text']
print(data.head(3))



### modif: train_test_split scikit-learn

# split into train and test dataframes

data_train, data_test = train_test_split(data, test_size=0.2, random_state=42, shuffle=True)

# or another 3rd dataframe of validation data

data_test, data_val = train_test_split(data_test, test_size=0.5, random_state=42, shuffle=True)

print(data_train.shape)
print(data_test.shape)
print(data_val.shape)

print(data_train.head(2))
print(data_test.head(2))
print(data_val.head(2))


# creation of different functions used to create new columns

def text_process(text):
    """
    function used to process text and remove stop words
    :param text:
    :return:
    """
    cleared_text = ' '.join([word for word in text.split() if word not in (stopwords.words('english'))])
    return cleared_text

# creation of a column without stop words

data_train['text_without_stopwords'] = data_train['text'].apply(text_process)
data_test['text_without_stopwords'] = data_test['text'].apply(text_process)
data_val['text_without_stopwords'] = data_val['text'].apply(text_process)


# print of the first lines of the dataframe

data_train.head(7)


# text-train contient les avis, sous forme de liste

text_train = data_train['text']
text_train = list(text_train)

text_test = data_test['text']
text_test = list(text_test)

porter = PorterStemmer()



# creation of different functions to manipulate the strings of words

def text_stem(text):
    """
    creation of a fonction for stemming & utilisation of the apply method
    :param text:
    :return:
    """
    stemmed_text = [porter.stem(y) for y in text]

    return stemmed_text


def text_tokenize(text):
    """
    creation of a function of tokenization
    :param text:
    :return:
    """
    tokenized_text = word_tokenize(text)

    return tokenized_text


def text_tokenize_and_stem(text):
    """
    creation of a function combining the previous two functions
    :param text:
    :return:
    """
    stemmed_text = [porter.stem(y) for y in word_tokenize(text)]

    return stemmed_text


# creation of new columns with functions word_tokenize and text_stem

data_train['nhi_stem'] = data_train['text_without_stopwords'].apply(text_tokenize_and_stem)
data_test['nhi_stem'] = data_test['text_without_stopwords'].apply(text_tokenize_and_stem)

data_train['stem'] = data_train['text'].apply(text_tokenize).apply(text_stem)
data_test['stem'] = data_test['text'].apply(text_tokenize).apply(text_stem)


def text_sentence(list_stems):
    """
    function to create sentence
    :param list_stems:
    :return:
    """
    sentence_text = ' '.join(stem for stem in list_stems)
    return sentence_text

data_train['full_stem_text'] = data_train['stem'].apply(text_sentence)
data_test['full_stem_text'] = data_test['stem'].apply(text_sentence)

data_train['sentence'] = data_train['stem'].apply(text_sentence)
data_test['sentence'] = data_test['stem'].apply(text_sentence)


# generation of an tf_idf object

tf_idf = TfidfVectorizer(stop_words ='english')


# tf_idf matrix on the basis of the training sample

train_corpus_tf_idf = tf_idf.fit_transform(data_train['full_stem_text'])
test_corpus_tf_idf = tf_idf.transform(data_test['full_stem_text'])


print(train_corpus_tf_idf.shape)
print("le fichier de train contient ", train_corpus_tf_idf.shape[0], " lignes/avis")
print("le vocabulaire sans les stop_words contient ", train_corpus_tf_idf.shape[1], " mots")

print(data_train.head(3))

# display the dimensions of the differents tf_idf matrices

print("dimensions de la matrice tf_idf sur le train = ", train_corpus_tf_idf.shape)
print("dimension du vecteur des labels sur le train = ", data_train['opinion'].shape)
print(" ")
print("dimensions de la matrice tf_idf sur le test = ", test_corpus_tf_idf.shape)
print("dimension du vecteur des labels sur le test = ", data_test['opinion'].shape)




# test of a logistic regression model

logreg = LogisticRegression()
logreg.fit(train_corpus_tf_idf,data_train['opinion'])

# we display different metrix

score_train = accuracy_score(logreg.predict(train_corpus_tf_idf),data_train['opinion'])
print("accuracy score sur le train = ", score_train)

pred = logreg.predict(test_corpus_tf_idf)

score = accuracy_score(pred,data_test['opinion'])
print("accuracy score sur le test = ", score)

score_precision = precision_score(pred,data_test['opinion'])
print("precision score sur le test = ", score_precision)

score_recall = recall_score(pred,data_test['opinion'])
print("recall score sur le test = ", score_recall)

score_f1 = f1_score(pred,data_test['opinion'])
print("f1 score sur le test = ", score_f1)

results = confusion_matrix(pred,data_test['opinion'])
print(results)


# here we create a new TFidfVecorizer with min_df & max_df options

tf_idf_V2 = TfidfVectorizer(stop_words ='english', min_df = 0.01, max_df = 0.5)

train_corpus_tf_idf_V2 = tf_idf_V2.fit_transform(data_train['full_stem_text'])
test_corpus_tf_idf_V2 = tf_idf_V2.transform(data_test['full_stem_text'])
train_corpus_tf_idf_V2
test_corpus_tf_idf_V2


# display of the dimensions of the tf_idf matrices involved on the train and test samples

print("dimensions de la matrice tf_idf sur le train = ", train_corpus_tf_idf_V2.shape)
print("dimension du vecteur des labels sur le train = ", data_train['opinion'].shape)
print(" ")
print("dimensions de la matrice tf_idf sur le test = ", test_corpus_tf_idf_V2.shape)
print("dimension du vecteur des labels sur le test = ", data_test['opinion'].shape)



# new logistic regression but with the min_df & max_df options

logreg = LogisticRegression()
logreg.fit(train_corpus_tf_idf_V2,data_train['opinion'])

score_train = accuracy_score(logreg.predict(train_corpus_tf_idf_V2),data_train['opinion'])
print("accuracy score sur le train = ", score_train)

pred = logreg.predict(test_corpus_tf_idf_V2)

score = accuracy_score(pred,data_test['opinion'])
print("accuracy score sur le test = ", score)

score_precision = precision_score(pred,data_test['opinion'])
print("precision score sur le test = ", score_precision)

score_recall = recall_score(pred,data_test['opinion'])
print("recall score sur le test = ", score_recall)

score_f1 = f1_score(pred,data_test['opinion'])
print("f1 score sur le test = ", score_f1)

results = confusion_matrix(pred,data_test['opinion'])
print(results)



range_max_df = [x * 0.1 for x in range(5, 10)]
range_max_df
range_min_df = [x * 0.01 for x in range(1, 6)]
range_min_df


# we create a matrix with the performances depending on the parameters min_df et max_df

mat_perf_train = np.zeros((5, 5))
mat_perf_test = np.zeros((5,5))
mat_vocabulaire = np.zeros((5,5))


# we sift throught the values of min_df & max_df to find the best parameters

for i in range_min_df:

    for j in range_max_df:
        tf_idf_V2 = TfidfVectorizer(stop_words='english', min_df=i, max_df=j)
        train_corpus_tf_idf_V2 = tf_idf_V2.fit_transform(data_train['full_stem_text'])
        test_corpus_tf_idf_V2 = tf_idf_V2.transform(data_test['full_stem_text'])
        train_corpus_tf_idf_V2
        test_corpus_tf_idf_V2

        logreg = LogisticRegression()
        logreg.fit(train_corpus_tf_idf_V2, data_train['opinion'])
        print(" ")
        print("seul de min_df = ", i, ", seuil de max_df = ", j)
        print("taille du vocabulaire : ", test_corpus_tf_idf_V2.shape[1])
        score_train = accuracy_score(logreg.predict(train_corpus_tf_idf_V2), data_train['opinion'])
        print("accuracy score sur le train = ", score_train)
        # train_min_df.append(score_train)

        pred = logreg.predict(test_corpus_tf_idf_V2)

        score_test = accuracy_score(pred, data_test['opinion'])
        print("accuracy score sur le test = ", score_test)
        # test_min_df.append(score_test)

        mat_perf_train[int(100 * i) - 1][int(10 * j) - 5] = score_train
        mat_perf_test[int(100 * i) - 1][int(10 * j) - 5] = score_test
        mat_vocabulaire[int(100 * i) - 1][int(10 * j) - 5] = test_corpus_tf_idf_V2.shape[1]


# we display the matrix with the performances on the train and test samples

print(range_min_df)
print(range_max_df)
print(" ")
print(" train sample: accuracy")
print(mat_perf_train)
print(" ")
print(" test sample: accuracy")
print(mat_perf_test)
print(" ")
print(" vocabulary size")
print(mat_vocabulaire.astype(int))


# Cross validation 10-fold for the logistic regression model

accuracy_logreg = cross_val_score(logreg, train_corpus_tf_idf, data_train['opinion'], cv=10, scoring='accuracy')
print(" ")
print("cross validation 10 fold")
print(accuracy_logreg)



x_axes = []

for i in range(1,11):
    x_axes.append(i)

x_axes




# test of a SVM model

# creation of a SVM object from the training data sample

clf_linearSVC = svm.LinearSVC()
clf_linearSVC.fit(train_corpus_tf_idf,data_train['opinion'])

score_svm_linear_train = accuracy_score(clf_linearSVC.predict(train_corpus_tf_idf),data_train['opinion'])
print("accuracy score sur le train = ", score_svm_linear_train)


# Test of different accuracy measures for the SVC

pred = clf_linearSVC.predict(test_corpus_tf_idf)
print(pred.shape)

score_svm_linear = accuracy_score(pred,data_test['opinion'])
print("accuracy score sur le test = ", score_svm_linear)

score_precision_svm_linear = precision_score(pred,data_test['opinion'])
print("precision score sur le test = ", score_precision_svm_linear)

score_recall_svm_linear = recall_score(pred,data_test['opinion'])
print("recall score sur le test = ", score_recall_svm_linear)

score_f1_svm_linear = f1_score(pred,data_test['opinion'])
print("f1 score sur le test = ", score_f1_svm_linear)

results_svm_linear = confusion_matrix(pred,data_test['opinion'])
print(results_svm_linear)


# Cross validation 10-fold for the linear SVC model

accuracy_linear_SVC = cross_val_score(clf_linearSVC, train_corpus_tf_idf, data_train['opinion'], cv=10, scoring='accuracy')
print(accuracy_linear_SVC)


# comparison between the logistic regression and linear SVC models

ax = plt.subplot(111)
ax.bar(x_axes, accuracy_logreg, width=0.3,color='b',align='center')
ax.bar([x+0.2 for x in x_axes], accuracy_linear_SVC ,width=0.2,color='g',align='center')
#ax.bar(x+0.2, k,width=0.2,color='r',align='center')
ax.autoscale(tight=True)
plt.xlabel("k-fold")
plt.ylabel("accuracy")
plt.title("10 fold pour reg log et linear SVC")
plt.legend(['bleue = log reg', 'vert = linear SVC'], loc="lower right")
plt.ylim(0.7)

plt.show()



# Optimisation par grid search de la régression logistique

param_grid = {'C': [0.01, 0.1, 1, 10, 100], 'penalty': ['l1', 'l2']}
model1 = LogisticRegression()
clf_logreg = GridSearchCV(model1, param_grid, cv=10, verbose=0)
best_model_logreg = clf_logreg.fit(train_corpus_tf_idf, data_train['opinion'])

print(best_model_logreg)

print(best_model_logreg.best_params_)


# meilleur modèle après grid search

model1_best = LogisticRegression(C=10, penalty='l2')
model1_best.fit(train_corpus_tf_idf,data_train['opinion'])


accuracy_logreg_optim = cross_val_score(model1_best, train_corpus_tf_idf, data_train['opinion'], cv=10, scoring='accuracy')
print(accuracy_logreg_optim)
print(accuracy_logreg)


# Représentation de la régression logistique standard et de la version optimisée

ax = plt.subplot(111)
ax.bar(x_axes, accuracy_logreg, width=0.3,color='b',align='center')
ax.bar([x+0.2 for x in x_axes], accuracy_logreg_optim ,width=0.2,color='g',align='center')
#ax.bar(x+0.2, k,width=0.2,color='r',align='center')
ax.autoscale(tight=True)
plt.xlabel("k-fold")
plt.ylabel("accuracy")
plt.title("10 fold pour reg log et reg log optim")
plt.legend(['bleue = log reg', 'vert = log reg optim'], loc="lower right")
plt.ylim(0.7)

plt.show()




# Optimisation par grid search sur le SVM

model2 = svm.LinearSVC()
param_grid_SVC={'C': [0.001, 0.01, 0.1, 1, 10], 'multi_class':['ovr'], 'loss': ['squared_hinge', 'hinge']}
clf_linearSVC = GridSearchCV(model2, param_grid_SVC, cv=10, verbose=0)
best_model_linearSVC = clf_linearSVC.fit(train_corpus_tf_idf, data_train['opinion'])

print(best_model_linearSVC)
print(best_model_linearSVC.best_params_)

model2_best = svm.LinearSVC(C = 1, loss='hinge', multi_class = 'ovr' )
model2_best.fit(train_corpus_tf_idf,data_train['opinion'])


accuracy_linearSVC_optim = cross_val_score(model2_best, train_corpus_tf_idf, data_train['opinion'], cv=10, scoring='accuracy')
print(accuracy_linearSVC_optim)
print(accuracy_linear_SVC)


# Représentation du SVM standard et de la version optimisée

ax = plt.subplot(111)
ax.bar(x_axes, accuracy_linear_SVC, width=0.3,color='b',align='center')
ax.bar([x+0.2 for x in x_axes], accuracy_linearSVC_optim ,width=0.2,color='g',align='center')
#ax.bar(x+0.2, k,width=0.2,color='r',align='center')
ax.autoscale(tight=True)
plt.xlabel("k-fold")
plt.ylabel("accuracy")
plt.title("10 fold pour linear SVC et linear SVC optim")
plt.legend(['bleue = log reg', 'vert = log reg optim'], loc="lower right")
plt.ylim(0.7)

plt.show()

