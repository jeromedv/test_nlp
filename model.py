#from data_processing import mat_tf_idf_train, data_train
from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score
from sklearn.metrics import r2_score
from sklearn.cross_validation import cross_val_score
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.model_selection import GridSearchCV
import numpy as np
import matplotlib.pyplot as plt


range_min_df = [x * 0.01 for x in range(1, 6)]
range_max_df = [x * 0.1 for x in range(5, 10)]
mat_perf_train = np.zeros((5, 5))
mat_perf_test = np.zeros((5, 5))
mat_vocabulaire = np.zeros((5, 5))


x_axes = []

for i in range(1,11):
    x_axes.append(i)


# we define a class Model (LogReg, SVM...) with different methods (fit, predict...)

class Model():

    def __init__(self, model_name='LogReg'):
        """
        constructor with LogReg model set up by default
        :param model_name:
        """

        if model_name == 'LogReg':
            self.model_in_use = LogisticRegression()
            self.param_grid = {'C': [0.01, 0.1, 1, 10, 100], 'penalty': ['l1', 'l2']}
            self.best_model = None
            self.model_nom = 'LogReg'

        elif model_name == 'SVM':
            self.model_in_use = LinearSVC()
            self.param_grid = {'C': [0.001, 0.01, 0.1, 1, 10], 'multi_class': ['ovr'], 'loss': ['squared_hinge', 'hinge']}
            self.best_model = None
            self.model_nom = 'SVM'


        elif model_name == 'RF':
            self.model_in_use = RandomForestClassifier()
           # self.param_grid = param_grid = {'bootstrap': [True], 'max_depth': [80, 90, 100, 110], 'max_features': [2, 3], 'min_samples_leaf': [3, 4, 5], 'min_samples_split': [8, 10, 12], 'n_estimators': [100, 200, 300, 1000]}
            self.param_grid = param_grid = {'max_depth': [100, 110],
                                            'max_features': [2, 3], 'min_samples_leaf': [3, 4, 5],
                                            'min_samples_split': [8, 10], 'n_estimators': [100, 200, 300]}
            self.best_model = None
            self.model_nom = 'RF'



    def fit_model(self, tf_idf, dataframe):
        """
        method to fit the model to a set of data
        :param tf_idf:
        :param dataframe:
        :return:
        """

        return self.model_in_use.fit(tf_idf, dataframe)

    def predict_model(self, test_sample):
        """
        method to predict on a sample the vector given by the model
        :param test_sample:
        :return:
        """

        return self.model_in_use.predict(test_sample)

    def cross_validation(self, tf_idf, dataframe):
        """
        method to do a crossvalidation
        :param tf_idf:
        :param dataframe:
        :return:
        """
        print("cross validation = ", cross_val_score(self.model_in_use, tf_idf, dataframe['opinion'], cv = 10, scoring='accuracy'))
        return cross_val_score(self.model_in_use, tf_idf, dataframe['opinion'], cv = 10, scoring='accuracy')


    def compute_accuracy(self, tf_idf, dataframe):
        """
        method to compute the accuracy of the model
        :param tf_idf:
        :param dataframe:
        :return:
        """

        score_train = accuracy_score(self.predict_model(tf_idf), dataframe['opinion'])
        print("accuracy score sur le train = ", score_train)
        return score_train


    def compute_grid_search(self, param_grid, tf_idf, dataframe):

        clf_model = GridSearchCV(self.model_in_use, self.param_grid, cv=10, verbose=0)
        self.best_model = clf_model.fit(tf_idf, dataframe['opinion'])
        #print(self.best_model)
        print("les meilleurs paramètres sont donnés par: ", self.best_model.best_params_)
        print("le modèle utilisé est = ", self.model_nom)


        if self.model_nom == 'LogReg':

            print("C = ", self.best_model.best_params_["C"])
            print("penalty = ", self.best_model.best_params_["penalty"])

        if self.model_nom == 'SVM':

            print("C = ", self.best_model.best_params_["C"])
            print("loss = ", self.best_model.best_params_["loss"])
            print("multi class = ", self.best_model.best_params_["multi_class"])


        if self.model_nom == 'RF':

            #print("bootstrap = ", self.best_model.best_params_["bootstrap"])
            print("max depth = ", self.best_model.best_params_["max_depth"])
            print("max features = ", self.best_model.best_params_["max_features"])
            print("min samples leaf = ", self.best_model.best_params_["min_samples_leaf"])
            print("min samples split = ", self.best_model.best_params_["min_samples_split"])
            print("nb estimators = ", self.best_model.best_params_["n_estimators"])

        return self.best_model



    def plot_histo(self, tf_idf, dataframe):


        # self.best_model_logreg = LogisticRegression(C = self.best_model_logreg.best_params_['C'], penalty = best_model_logreg.best_params_['penalty'] )
        # best.fit(tf_idfn, dataframe)
        accuracy_logreg_optim = cross_val_score(self.best_model, tf_idf, dataframe['opinion'], cv=10,
                                                 scoring='accuracy')

        ax = plt.subplot(111)
        ax.bar(x_axes, self.cross_validation(tf_idf, dataframe), width=0.3, color='b', align='center')
        ax.bar([x + 0.2 for x in x_axes], accuracy_logreg_optim, width=0.2, color='g', align='center')
        ax.autoscale(tight=True)
        plt.xlabel("k-fold")
        plt.ylabel("accuracy")
        plt.title("10 fold pour modèle standard et optimisé")
        plt.legend(['bleue = modèle standard', 'vert = modèle optimisé'], loc="lower right")
        plt.ylim(0.6, 1)

        #print(self.compute_accuracy(tf_idf, dataframe))


        plt.show()




    def compute_min_max_df(self, dataframe_train, dataframe_test):




        for i in range_min_df:

            for j in range_max_df:
                tf_idf_V2 = TfidfVectorizer(stop_words='english', min_df=i, max_df=j)
                train_corpus_tf_idf_V2 = tf_idf_V2.fit_transform(dataframe_train['full_stem_text'])
                test_corpus_tf_idf_V2 = tf_idf_V2.transform(dataframe_test['full_stem_text'])

                #logreg = Model('LogReg')
                model = Model(self.model_nom)
                model.fit_model(train_corpus_tf_idf_V2, dataframe_train['opinion'])
                # model = LogisticRegression()
                # model.fit(train_corpus_tf_idf_V2, data_train['opinion'])
                print(" ")
                print("seuil de min_df = ", i, ", seuil de max_df = ", j)
                print("taille du vocabulaire : ", test_corpus_tf_idf_V2.shape[1])
                score_train = accuracy_score(model.predict_model(train_corpus_tf_idf_V2), dataframe_train['opinion'])
                print("accuracy score sur le train = ", score_train)
                # train_min_df.append(score_train)

                pred = model.predict_model(test_corpus_tf_idf_V2)
                pred.shape

                score_test = accuracy_score(pred, dataframe_test['opinion'])
                print("accuracy score sur le test = ", score_test)
                # test_min_df.append(score_test)

                mat_perf_train[int(100 * i) - 1][int(10 * j) - 5] = score_train
                mat_perf_test[int(100 * i) - 1][int(10 * j) - 5] = score_test
                mat_vocabulaire[int(100 * i) - 1][int(10 * j) - 5] = test_corpus_tf_idf_V2.shape[1]


#######


if __name__ == '__main__':
    print("debut")

    my_model_logReg = Model('LogReg')
    #my_model_logReg.fit_model(mat_tf_idf_train,data_train['opinion'])
    print("fin")
