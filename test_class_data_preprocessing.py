from data_processing import DataProcessing
from model import Model
import pandas
import pickle

if __name__ == '__main__':
    print('test_class_data_ debut')
    data = pandas.read_csv('data/opinions.csv', sep='\t+', engine='python')
    data['"opinion'] = data['"opinion'].str[1:]
    data['"opinion'] = data['"opinion'].astype(int)
    data.columns = ['opinion', 'text']

    processor = DataProcessing()

    data_train, data_test, data_val = processor.data_splitting_train_test_val(data)

    #print(data_train.shape)
    #print(data_test.shape)
    #print(data_val.shape)

    #print(data_train.head(2))
    #print(data_test.head(2))
    #print(data_val.head(2))

    data_train = processor.remove_stopword(data_train)
    data_train = processor.stem_words(data_train)
    data_train = processor.text_sentence(data_train)

    data_test = processor.remove_stopword(data_test)
    data_test = processor.stem_words(data_test)
    data_test = processor.text_sentence(data_test)




    #mat_tf_idf_train = processor.create_tf_idf_matrix_train(data_train)
    #mat_tf_idf_test = processor.create_tf_idf_matrix_test(data_test)

    mat_tf_idf_train, mat_tf_idf_test = processor.create_tf_idf_matrix(data_train, data_test)

    # store the content of data_train and data_test matrices

    with open("C:/Users/jerome.divac/Stage/NLP/tf_idf/data_train.pkl", 'wb') as handle:
        pickle.dump(data_train, handle)

    with open("C:/Users/jerome.divac/Stage/NLP/tf_idf/data_test.pkl", 'wb') as handle:
        pickle.dump(data_test, handle)

    # store the content of tf_idf matrices

    with open("C:/Users/jerome.divac/Stage/NLP/tf_idf/mat_tf_idf_train.pkl", 'wb') as handle:
        pickle.dump(mat_tf_idf_train, handle)


    with open("C:/Users/jerome.divac/Stage/NLP/tf_idf/mat_tf_idf_test.pkl", 'wb') as handle:
        pickle.dump(mat_tf_idf_test, handle)


    print(data_test.head(3))
    print(data.columns)

