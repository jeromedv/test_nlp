import pandas
from sklearn.model_selection import train_test_split
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords

from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer



import numpy as np
desired_width=320
pandas.set_option('display.width', desired_width)
np.set_printoptions(linewidth=desired_width)
pandas.set_option('display.max_columns',20)



# creation of a class DataProcessing with the methods to manipulate the initial data

class DataProcessing():

    def __init__(self):
        self.test = 0

    def data_splitting_train_test_val(self, data, test_per = 0.2, val_per = 0.5):
        """
        function to split the data into train, test and validation sets
        :return:
        """

        print(self.test)

        ### modif: train_test_split scikit-learn
        # split into train and test dataframes

        data_train, data_test = train_test_split(data, test_size=test_per, random_state=42, shuffle=True)
        #data_train, data_test = train_test_split(data, test_size=test_per, shuffle=True)

        # or another 3rd dataframe of validation data

        data_test, data_val = train_test_split(data_test, test_size=val_per, random_state=42, shuffle=True)
        #data_test, data_val = train_test_split(data_test, test_size=val_per, shuffle=True)

        return data_train, data_test, data_val

    # def remove_stopword_function(self, text):
    #     """
    #     function used to process text and remove stop words
    #     :param text:
    #     :return:
    #     """
    #     cleared_text = ' '.join([word for word in text.split() if word not in (stopwords.words('english'))])
    #     return cleared_text

    def remove_stopword(self, dataframe):
        """
        function to remeove the stopword from a dataframe
        :param dataframe:
        :return:
        """
        dataframe['text_without_stopwords'] = dataframe['text'].apply(lambda text: ' '.join([word for word in text.split() if word not in (stopwords.words('english'))]))
        return dataframe


    # def text_stem(self, text):
    #     """
    #     creation of a fonction for stemming & utilisation of the apply method
    #     :param text:
    #     :return:
    #     """
    #     porter = PorterStemmer()
    #     stemmed_text = [porter.stem(y) for y in text]
    #
    #     return stemmed_text

    # def text_tokenize(self, text):
    #     """
    #     creation of a function of tokenization
    #     :param text:
    #     :return:
    #     """
    #     tokenized_text = word_tokenize(text)
    #
    #     return tokenized_text

    # def text_tokenize_and_stem(self, text):
    #     """
    #     creation of a function combining the previous two functions
    #     :param text:
    #     :return:
    #     """
    #     porter = PorterStemmer()
    #     stemmed_text = [porter.stem(y) for y in word_tokenize(text)]
    #
    #     return stemmed_text

    def stem_words(self, dataframe):

        """
        funtion to create a colupmn with the stemming of the words
        :param dataframe:
        :return:
        """

        porter = PorterStemmer()
        dataframe['nhi_stem'] = dataframe['text_without_stopwords'].apply(lambda x: [porter.stem(y) for y in word_tokenize(x)])
        #dataframe['stem'] = dataframe['text'].apply(text_tokenize).apply(text_stem)
        dataframe['stem'] = dataframe['text'].apply(lambda x: word_tokenize(x)).apply(lambda x: [porter.stem(y) for y in x])
        return dataframe



    def text_sentence(self, dataframe):

        """
        function to create column with the sentence without stop words, and with stemming
        :param dataframe:
        :return:
        """

        dataframe['full_stem_text'] = dataframe['stem'].apply(lambda x: ' '.join(stem for stem in x))

        #sentence_text = ' '.join(stem for stem in list_stems)
        return dataframe

    #data_train['full_stem_text'] = data_train['stem'].apply(text_sentence)
    #data_test['full_stem_text'] = data_test['stem'].apply(text_sentence)






# Création d'une liste contenant les différents avis sur l'échantillon de train/test



    # def create_tf_idf_matrix_train(self, dataframe):
    #
    #     tf_idf = TfidfVectorizer(stop_words='english')
    #     train_corpus_tf_idf = tf_idf.fit_transform(dataframe['full_stem_text'])
    #     print("dimensions de la matrice tf_idf sur le train = ", train_corpus_tf_idf.shape)
    #     print("dimension du vecteur des labels sur le train = ", data_train['opinion'].shape)
    #
    #     return train_corpus_tf_idf
    #
    #
    # def create_tf_idf_matrix_test(self, dataframe):
    #
    #     tf_idf = TfidfVectorizer(stop_words='english')
    #     test_corpus_tf_idf = tf_idf.transform(dataframe['full_stem_text'])
    #     print("dimensions de la matrice tf_idf sur le test = ", test_corpus_tf_idf.shape)
    #     print("dimension du vecteur des labels sur le test = ", data_test['opinion'].shape)
    #
    #     return test_corpus_tf_idf



    def create_tf_idf_matrix(self, dataframe_train, dataframe_test):

        """
        function to compute a tf_idf matrix
        :param dataframe_train:
        :param dataframe_test:
        :return:
        """

        tf_idf = TfidfVectorizer(stop_words='english')
        train_corpus_tf_idf = tf_idf.fit_transform(dataframe_train['full_stem_text'])
        print("dimensions de la matrice tf_idf sur le train = ", train_corpus_tf_idf.shape)
        print("dimension du vecteur des labels sur le train = ", dataframe_train['opinion'].shape)
        test_corpus_tf_idf = tf_idf.transform(dataframe_test['full_stem_text'])
        print("dimensions de la matrice tf_idf sur le test = ", test_corpus_tf_idf.shape)
        print("dimension du vecteur des labels sur le test = ", dataframe_test['opinion'].shape)

        return train_corpus_tf_idf, test_corpus_tf_idf


# data = pandas.read_csv('data/opinions.csv', sep='\t+', engine='python')
# data['"opinion'] = data['"opinion'].str[1:]
# data['"opinion'] = data['"opinion'].astype(int)
# data.columns = ['opinion', 'text']
#
# processor = DataProcessing()
#
# data_train, data_test, data_val = processor.data_splitting_train_test_val(data)
#
# # print(data_train.shape)
# # print(data_test.shape)
# # print(data_val.shape)
#
# # print(data_train.head(2))
# # print(data_test.head(2))
# # print(data_val.head(2))
#
# data_train = processor.remove_stopword(data_train)
# data_train = processor.stem_words(data_train)
# data_train = processor.text_sentence(data_train)
#
# data_test = processor.remove_stopword(data_test)
# data_test = processor.stem_words(data_test)
# data_test = processor.text_sentence(data_test)
#
# # mat_tf_idf_train = processor.create_tf_idf_matrix_train(data_train)
# # mat_tf_idf_test = processor.create_tf_idf_matrix_test(data_test)
#
# mat_tf_idf_train, mat_tf_idf_test = processor.create_tf_idf_matrix(data_train, data_test)
#
# print(data_test.head(3))
# print(data.columns)


if __name__ == '__main__':

    data = pandas.read_csv('data/opinions.csv', sep='\t+', engine='python')
    data['"opinion'] = data['"opinion'].str[1:]
    data['"opinion'] = data['"opinion'].astype(int)
    data.columns = ['opinion', 'text']

    processor = DataProcessing()

    data_train, data_test, data_val = processor.data_splitting_train_test_val(data)

    #print(data_train.shape)
    #print(data_test.shape)
    #print(data_val.shape)

    #print(data_train.head(2))
    #print(data_test.head(2))
    #print(data_val.head(2))

    data_train = processor.remove_stopword(data_train)
    data_train = processor.stem_words(data_train)
    data_train = processor.text_sentence(data_train)

    data_test = processor.remove_stopword(data_test)
    data_test = processor.stem_words(data_test)
    data_test = processor.text_sentence(data_test)


    #mat_tf_idf_train = processor.create_tf_idf_matrix_train(data_train)
    #mat_tf_idf_test = processor.create_tf_idf_matrix_test(data_test)

    mat_tf_idf_train, mat_tf_idf_test = processor.create_tf_idf_matrix(data_train, data_test)


    print(data_test.head(3))
    print(data.columns)



